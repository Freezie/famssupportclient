<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportSolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_solutions', function (Blueprint $table) {
            $table->id();
            $table->string('support_ticket_no');
            $table->string('respondent_email');
            $table->string('responded_by');                             
            $table->longText('support_description');
            $table->string('man_hours');            
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_solutions');
    }
}
