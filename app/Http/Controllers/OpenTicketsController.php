<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SupportTicket;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\SupportSolution;
use Illuminate\Support\Facades\DB;

class OpenTicketsController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth'],['verified']);
    }
    public function index(){
        $user_name = Auth::user()->name;
        if(session('success_message')){
            Alert::success('Hi '.$user_name, session('success_message'));
        }
        $current_email = Auth::user()->email;
        $open_tickets = SupportTicket::where('status', '1')->get();
        $open_tickets_mine = SupportTicket::where('status', '1')->where('contact_email', $current_email)->get();
        
        return view ('opentickets.index',compact('open_tickets','open_tickets_mine'));

    }
    public function show($id){
        $current_email = Auth::user()->email;
        $supportticket = SupportTicket::find($id);
        $tickets = SupportTicket::all();
        return view('opentickets.ticket-details',compact('supportticket','tickets'));
    }
    public function edit($id){
        $current_email = Auth::user()->email;
        $supportticket = SupportTicket::find($id);
        $tickets = SupportTicket::all();
        return view('opentickets.respond-ticket',compact('supportticket','tickets'));
    }

    public function update(Request $request)
    {
        $user_name = Auth::user()->name;
        if (session('success_message')) {
            Alert::success('Hi ' . $user_name, session('success_message'));
        }
        // new SupportTicket()
        $ticket_solution = new SupportSolution();
        // $ticket_solution = SupportSolution::find($id);
        $ticket_num = $ticket_solution->support_ticket_no = $request->get('support_ticket_no');
        $ticket_solution->support_ticket_no = $request->get('support_ticket_no');
        $ticket_solution->responded_by = $request->get('responded_by');
        $ticket_solution->respondent_email = $request->get('respondent_email');
        $ticket_solution->support_description = $request->get('support_description');
        $ticket_solution->status = $request->get('status');
        $ticket_solution->save();

        // $new_status = '22';
        // $ticket_solution = SupportTicket::find($id);
        // $ticket_solution->status = $new_status;
        // $ticket_solution->save();
        // $ticket_solution = DB::select('select * from support_solutions where support_ticket_no=20200323040345');
        // $ticket_solution->save();
        // DB::table('support_tickets')->where('support_ticket_no', 1)->update(array(
        //     'support_ticket_no' => $username,
        // ));

        DB::table('support_tickets')
            ->where('support_ticket_no', $ticket_num)
            ->update(['status' => 2]);

        // $owner_email = SupportTicket::select('contact_email')->where('support_ticket_no', $ticket_num)->get();
        $owner_email = DB::table('support_tickets')
            ->where('support_ticket_no', '=', $ticket_num)
            ->pluck('contact_email');

        // // $final_str = [];
        // // preg_match_all('`"([^"]*)"`', $owner_email, $final_str);
        // // $final_str[1]; //this is what you want
        // // $email_email = $final_str[1];

       

        // // // $full_name = "John Doe";
        // // // $name = explode(' ', $full_name);
        // // // $first_name = $name[0];
        // // // $last_name = $name[1];

        // // // $full_name = "John Doe";
        // // $owner_email = explode(' ', $owner_email);
        // // $owner_email = $owner_email[0];
        // // // $owner_email = $name[1];


        // $myEmail = 'harmfullbe@gmail.com';
        // $details = [
        //     // 'subject' => 'Ticket Number: '.$support_ticket->support_ticket_no,
        //     'title' => 'Support request for: ' .$ticket_num,
        //     'url' => 'http://127.0.0.1:8000/login',
        //     'message' => 'We have Responded to the request. '
        // ];
        // Mail::to($myEmail)->send(new SupportTicketMail($details));



        return redirect('opentickets')->withSuccessMessage('Thank you for responding to the ticket.');
    }
    
}
