<?php

namespace App\Http\Controllers;

use App\CancelledTicket;
use Illuminate\Http\Request;

class CancelledTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CancelledTicket  $cancelledTicket
     * @return \Illuminate\Http\Response
     */
    public function show(CancelledTicket $cancelledTicket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CancelledTicket  $cancelledTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(CancelledTicket $cancelledTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CancelledTicket  $cancelledTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CancelledTicket $cancelledTicket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CancelledTicket  $cancelledTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(CancelledTicket $cancelledTicket)
    {
        //
    }
}
