<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SupportTicket;
use App\Client;
use App\Project;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Mail;

use App\Mail\MyDemoMail;
use App\Mail\SupportTeamMail;
use App\Mail\UserSupportTicketMail;
use App\Mail\SupportTicketMail;
// use Mail;

class GuestTicketController extends Controller
{
    //

    public function index()
    {
        if (session('success_message')) {
            Alert::success('Hi', session('success_message'));
        }
        if (session('error_message')) {
            Alert::error('Hello', session('error_message'));
        }
        if (session('warning_message')) {
            Alert::warning('Hi', session('warning_message'));
        }
        $projects = Project::all();
        return view('welcome', compact('projects'));
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'support_ticket_no' => 'required',
                'contact_email' => 'required',
                'company' => 'required',
                'application' => 'required',
                'priority' => 'required',
                'status' => 'required|integer',
                'attachement' => 'max:5120'
            ]);
            $cover = $request->file('attachement');
            if (!empty($cover)) {
                $extension = $cover->getClientOriginalExtension();
                Storage::disk('public')->put($cover->getFilename() . '.' . $extension,  File::get($cover));
                $support_ticket = new SupportTicket();
                $support_ticket->support_ticket_no = $request->support_ticket_no;
                $support_ticket->contact_email = $request->contact_email;
                $support_ticket->company = $request->company;
                $support_ticket->application = $request->application;
                $support_ticket->priority = $request->priority;
                $support_ticket->support_category = $request->support_category;
                $support_ticket->support_description = $request->support_description;
                $support_ticket->created_by = $request->created_by;
                $support_ticket->status = $request->status;
                $support_ticket->mime = $cover->getClientMimeType();
                $support_ticket->original_filename = $cover->getClientOriginalName();
                $support_ticket->filename = $cover->getFilename() . '.' . $extension;
            } else {
                $support_ticket = new SupportTicket();
                $support_ticket->support_ticket_no = $request->support_ticket_no;
                $support_ticket->contact_email = $request->contact_email;
                $support_ticket->company = $request->company;
                $support_ticket->application = $request->application;
                $support_ticket->priority = $request->priority;
                $support_ticket->support_category = $request->support_category;
                $support_ticket->support_description = $request->support_description;
                $support_ticket->created_by = $request->created_by;
                $support_ticket->status = $request->status;
            }
            $support_ticket->save();
        } catch (\Exception $e) {
            return redirect('/')->withErrorMessage('Kindly make sure you have inputs for the required fields and try again');
        }

        //Check if the email exists.
        $defaultpassword = 'Newuser@2020';
        $defaultuser = 'user';

        try {

            $new_user = new Client();
            $new_user->name = $request->created_by;
            $new_user->email = $request->contact_email;
            $new_user->password = $defaultpassword;
            $new_user->user_type = $defaultuser;
            $new_user->save();

            //Post in users Table.
            $new_user = new User();
            $new_user->name = $request->created_by;
            $new_user->email = $request->contact_email;
            $new_user->password = Hash::make($defaultpassword);
            $new_user->user_type = $defaultuser;
            $new_user->save();

            $myEmail = $new_user->email;
            $details = [
                'title' => 'Support request for: ' . $support_ticket->support_ticket_no,
                'url' => 'http://dev.tamarix.co.ke/login',
                'message' => 'We have received your support request and we are working on it. 
                Kindly track your ticket through your account. login with the email you used in contact email field and password as: ' . $defaultpassword,
                'subject' => ''
            ];
            Mail::to($myEmail)->send(new UserSupportTicketMail($details));
        } catch (\Exception $e) {

            try {
                $myEmail = $new_user->email;
                $details = [
                    'title' => 'Support request for: ' . $support_ticket->support_ticket_no,
                    'url' => 'http://dev.tamarix.co.ke/login',
                    'message' => 'We have received your support request and we are working on it. 
                Kindly track your ticket through your account.',
                    'subject' => ''
                ];
                Mail::to($myEmail)->send(new UserSupportTicketMail($details));

                //send to staff
                $created_by = $support_ticket->created_by;
                $user_email = $support_ticket->contact_email;
                $request = $support_ticket->support_description;
                $myEmails = ['humphreybrian43@gmail.com', 'support@tamarix.co.ke'];
                $details = [
                    // 'subject' => 'Ticket Number: '.$support_ticket->support_ticket_no,
                    'title' => 'Support request for: ' . $support_ticket->support_ticket_no,
                    'url' => 'http://dev.tamarix.co.ke/login',
                    'message' => 'Kindly respond to this support request created by ' . $created_by . ' with email ' . $user_email . '. The request is: ',
                    'description' => $request
                ];
                Mail::to($myEmails)->send(new SupportTeamMail($details));
            } catch (\Exception $e) {
                return redirect('/')->withWarningMessage('Your ticket was submited successfully. No email fot you, the Mail service seems to be down.');
            }

            return redirect('/')->withSuccessMessage('Your ticket was submited successfully. You have an account with that contact email. kindly login to track your ticket');
        }


        $created_by = $support_ticket->created_by;
        $user_email = $support_ticket->contact_email;
        $request = $support_ticket->support_description;
        $myEmails = ['humphreybrian43@gmail.com', 'support@tamarix.co.ke'];
        $details = [
            // 'subject' => 'Ticket Number: '.$support_ticket->support_ticket_no,
            'title' => 'Support request for: ' . $support_ticket->support_ticket_no,
            'url' => 'http://dev.tamarix.co.ke/login',
            'message' => 'Kindly respond to this support request created by ' . $created_by . ' with email ' . $user_email . '. The request is: ',
            'description' => $request
        ];
        Mail::to($myEmails)->send(new SupportTeamMail($details));

        return redirect('/')->withSuccessMessage('Support ticket submited successfully. 
        We have created a support account with your contact email and the credentials sent to your email.');
        // return $support_ticket;

    }
}
