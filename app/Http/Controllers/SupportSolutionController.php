<?php

namespace App\Http\Controllers;

use App\SupportSolution;
use Illuminate\Http\Request;

class SupportSolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupportSolution  $supportSolution
     * @return \Illuminate\Http\Response
     */
    public function show(SupportSolution $supportSolution)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupportSolution  $supportSolution
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportSolution $supportSolution)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupportSolution  $supportSolution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportSolution $supportSolution)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupportSolution  $supportSolution
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportSolution $supportSolution)
    {
        //
    }
}
