<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth'], ['verified']);
    }
    public function index()
    {
        //
        $user_name = Auth::user()->name;
        if (session('success_message')) {
            Alert::success('Hi ' . $user_name, session('success_message'));
        }
        if (session('error_message')) {
            Alert::error('Hello ' . $user_name, session('error_message'));
        }
        $projects = Project::all();
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (session('error_message')) {
            Alert::error('Hi ', session('error_message'));
        }

        try {
            $new_project = new Project();
            $new_project->company = $request->get('company');
            $new_project->project = $request->get('project');
            $new_project->start_date = $request->get('start_date');
            $new_project->end_date = $request->get('end_date');
            $new_project->created_by = $request->get('created_by');
            $new_project->save();
        } catch (\Exception $e) {
            return redirect('projects')->withErrorMessage('Check your inputs or the email service is down.');
        }

        return redirect('projects')->withSuccessMessage('Project details created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
