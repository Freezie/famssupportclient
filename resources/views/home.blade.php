<!DOCTYPE html>
<html lang="en-US">

<head>
    @include('shared.head')
</head>

<body>
    <div id="page-container">
        @include('shared.nav')
        @include('shared.header')
        <aside id="right-sidebar" class="r_sidebar">
            <div class="content-wrapper"><a href="javascript:void(0)" class="close-btn">
                    <span class="ti-close"></span>
                </a>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="today-tab" data-toggle="tab" href="#today" aria-expanded="true">Today</a></li>
                    <li class="nav-item"><a class="nav-link" id="setting-tab" data-toggle="tab" href="#setting">Setting</a>
                    </li>
                </ul>
                <div class="tab-content sidebar-wrapper scrollbar-inner" id="myTabContent">
                    <div class="tab-pane fade show active" id="today">
                        <div class="today-date"><span class="strong"><span id="prMonth"></span> <span id="prDate"></span></span>, <span id="prYear"></span>
                            <span id="prDay"></span>
                        </div>
                        <div class="block-tab">
                            <div class="block-title"><span class="ti-time"></span> Schedule
                            </div>
                            <ul class="schedule-list list-unstyled">
                                <li>
                                    <div class="time">
                                        09.00<span>AM</span></div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Briefing with product division
                                        <span class="location"><span class="ti-location-pin"></span> New York, NA</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">11.00<span>AM</span>
                                    </div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Meeting with client
                                        <span class="location"><span class="ti-location-pin"></span> Client office</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">
                                        01.30<span>PM</span></div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Project commisioning<span class="location"><span class="ti-location-pin"></span> Office</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">
                                        04.00<span>AM</span></div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Public discussion at office<span class="location"><span class="ti-location-pin"></span> Cafetaria</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="block-tab">
                            <div class="block-title"><span class="ti-flag"></span> Activity Log
                            </div>
                            <ul class="activity-list list-unstyled">
                                <li>
                                    <div class="icon">
                                        <span class="ti-image"></span>
                                    </div>
                                    <div class="log-info">
                                        Photo profile has been updated<small>2 min ago</small>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><span class="ti-email"></span>
                                    </div>
                                    <div class="log-info">
                                        New email to <strong>John Cenna</strong> sent<small>4 hrs ago</small>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><span class="ti-email"></span>
                                    </div>
                                    <div class="log-info">You compose new email
                                        <small>6 hrs ago</small></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <span class="ti-email"></span></div>
                                    <div class="log-info">You compose new email
                                        <small>1 day ago</small></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="setting">
                        <ul class="setting-list list-unstyled">
                            <li class="header">
                                Main system</li>
                            <li>
                                <div class="setting-name">
                                    Cancelled Tickets</div>
                                <div class="switch"><input type="checkbox" class="js-switch" data-size="small" checked />
                                </div>
                            </li>
                            <li>
                                <div class="setting-name">
                                    Auto updates
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" />
                                </div>
                            </li>
                            <li>
                                <div class="setting-name">
                                    Location
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" checked />
                                </div>
                            </li>
                            <li class="header">Assistant
                            </li>
                            <li>
                                <div class="setting-name">
                                    Show Assistant</div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" />
                                </div>
                            </li>
                            <li class="header">
                                Appearances
                            </li>
                            <li>
                                <div class="setting-name">
                                    Save history
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" checked />
                                </div>
                            </li>
                            <li>
                                <div class="setting-name">
                                    Quick results
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>


        <!-- MAIN CONTAINER -->
        @can('isAdmin')
        <main id="main-container">
            <div class="content">

                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- All tickets  -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:#000000;">
                                <span class="ti-bag"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$all_tickets}}</span></div>
                                <span class="desc">All tickets </span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End All tickets  -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Open Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:green;">
                                <span class="ti-wallet"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$open_tickets}}</span></div>
                                <span class="desc">Open Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Open Tickets -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Closed Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:orange;">
                                <span class="ti-email"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$closed_tickets}}</span></div>
                                <span class="desc">Closed Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Closed Tickets -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Cancelled Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:red;">
                                <span class="ti-bell"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$cancelled_tickets}}</span></div>
                                <span class="desc">Cancelled Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Cancelled Tickets -->

                    </div><!-- .col -->
                </div><!-- .row -->

                <div class="row">
                    <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-9 dev-resource-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-options">
                                    <button type="button" class="btn-option" id="refresh1">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                    <button type="button" class="btn-option" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Hide</a>
                                    </div>
                                </div>
                                <div class="card-header-inside">
                                    Device Resource
                                </div>

                                <div class="refresh-container">
                                    <i class="refresh-spinner fa fa-circle-o-notch fa-spin fa-5x"></i>
                                </div>

                                <canvas id="chart_verticalBar" style="width: 100%;"></canvas>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-9 dev-resource-card">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="card-header-inside">
                                    Tickets
                                </div>
                                <table id="example1" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Ticket Number</th>
                                            <th>Company</th>
                                            <th>Application</th>
                                            <!-- <th>Ticket Created by</th> -->
                                            <th>Creation Date</th>
                                            <!-- <th>Contact Email</th> -->
                                            <!-- <th>Support Title</th> -->
                                            <!-- <th>Support Description</th> -->

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($alltickets as $mytickets)
                                        <tr>
                                            <td>{{ $mytickets->support_ticket_no }}</td>
                                            <td>{{ $mytickets->company }}</td>
                                            <td>{{ $mytickets->application }}</td>
                                            <!-- <td>{{ $mytickets->created_by}}</td> -->
                                            <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
                                            <!-- <td>{{ $mytickets->contact_email }}</td> -->
                                            <!-- <td>{{ $mytickets->support_category }}</td> -->
                                            <!-- <td>{{ $mytickets->support_description }}</td> -->
                                        </tr>
                                        @empty
                                        <tr>
                                            <p>No Data availabe</p>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-3 server-load">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-header-inside">
                                    Ticket Status
                                </div>
                                <div class="pie-chart-container">
                                    <canvas id="chart_pie" style="width: 100%;"></canvas>
                                </div>
                                <table class="table table-server mt-3">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Tickets</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width:45%">Closed</td>
                                            <td>{{$closed_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Open</td>
                                            <td>{{$open_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cancelled</td>
                                            <td>{{$cancelled_tickets}}
                                                <!-- <span class="badge badge-info">absolete</span> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @endcan
        @can('isCertify')
        <main id="main-container">
            <div class="content">

                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- All tickets  -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:#000000;">
                                <span class="ti-bag"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$all_tickets}}</span></div>
                                <span class="desc">All tickets </span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End All tickets  -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Open Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:green;">
                                <span class="ti-wallet"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$open_tickets}}</span></div>
                                <span class="desc">Open Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Open Tickets -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Closed Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:orange;">
                                <span class="ti-email"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$closed_tickets}}</span></div>
                                <span class="desc">Closed Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Closed Tickets -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Cancelled Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:red;">
                                <span class="ti-bell"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$cancelled_tickets}}</span></div>
                                <span class="desc">Cancelled Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Cancelled Tickets -->

                    </div><!-- .col -->
                </div><!-- .row -->

                <div class="row">
                    <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-9 dev-resource-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-options">
                                    <button type="button" class="btn-option" id="refresh1">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                    <button type="button" class="btn-option" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Hide</a>
                                    </div>
                                </div>
                                <div class="card-header-inside">
                                    Device Resource
                                </div>

                                <div class="refresh-container">
                                    <i class="refresh-spinner fa fa-circle-o-notch fa-spin fa-5x"></i>
                                </div>

                                <canvas id="chart_verticalBar" style="width: 100%;"></canvas>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-9 dev-resource-card">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="card-header-inside">
                                    Tickets
                                </div>
                                <table id="example1" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Ticket Number</th>
                                            <th>Company</th>
                                            <th>Application</th>
                                            <!-- <th>Ticket Created by</th> -->
                                            <th>Creation Date</th>
                                            <!-- <th>Contact Email</th> -->
                                            <!-- <th>Support Title</th> -->
                                            <!-- <th>Support Description</th> -->

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($alltickets as $mytickets)
                                        <tr>
                                            <td>{{ $mytickets->support_ticket_no }}</td>
                                            <td>{{ $mytickets->company }}</td>
                                            <td>{{ $mytickets->application }}</td>
                                            <!-- <td>{{ $mytickets->created_by}}</td> -->
                                            <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
                                            <!-- <td>{{ $mytickets->contact_email }}</td> -->
                                            <!-- <td>{{ $mytickets->support_category }}</td> -->
                                            <!-- <td>{{ $mytickets->support_description }}</td> -->
                                        </tr>
                                        @empty
                                        <tr>
                                            <p>No Data availabe</p>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-3 server-load">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-header-inside">
                                    Ticket Status
                                </div>
                                <div class="pie-chart-container">
                                    <canvas id="chart_pie" style="width: 100%;"></canvas>
                                </div>
                                <table class="table table-server mt-3">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Tickets</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width:45%">Closed</td>
                                            <td>{{$closed_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Open</td>
                                            <td>{{$open_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cancelled</td>
                                            <td>{{$cancelled_tickets}}
                                                <!-- <span class="badge badge-info">absolete</span> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @endcan
        @can('isUser')
        <main id="main-container">
            <div class="content">

                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- All tickets  -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:#000000;">
                                <span class="ti-bag"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$all_tickets_mine}}</span></div>
                                <span class="desc">All tickets </span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End All tickets  -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Open Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:green;">
                                <span class="ti-wallet"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$open_tickets_mine}}</span></div>
                                <span class="desc">Open Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Open Tickets -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Closed Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:orange;">
                                <span class="ti-email"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$closed_tickets_mine}}</span></div>
                                <span class="desc">Closed Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Closed Tickets -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Cancelled Tickets -->
                        <div class="card stats-card">
                            <div class="stats-icon" style="background:red;">
                                <span class="ti-bell"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">{{$cancelled_tickets_mine}}</span></div>
                                <span class="desc">Cancelled Tickets</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Cancelled Tickets -->

                    </div><!-- .col -->
                </div><!-- .row -->

                <div class="row">
                    <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-9 dev-resource-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-options">
                                    <button type="button" class="btn-option" id="refresh1">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                    <button type="button" class="btn-option" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Hide</a>
                                    </div>
                                </div>
                                <div class="card-header-inside">
                                    Device Resource
                                </div>

                                <div class="refresh-container">
                                    <i class="refresh-spinner fa fa-circle-o-notch fa-spin fa-5x"></i>
                                </div>

                                <canvas id="chart_verticalBar" style="width: 100%;"></canvas>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-9 dev-resource-card">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="card-header-inside">
                                    Tickets
                                </div>
                                <table id="example1" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Ticket Number</th>
                                            <th>Company</th>
                                            <th>Application</th>
                                            <!-- <th>Ticket Created by</th> -->
                                            <th>Creation Date</th>
                                            <!-- <th>Contact Email</th> -->
                                            <!-- <th>Support Title</th> -->
                                            <!-- <th>Support Description</th> -->

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($alltickets_mine as $mytickets)
                                        <tr>
                                            <td>{{ $mytickets->support_ticket_no }}</td>
                                            <td>{{ $mytickets->company }}</td>
                                            <td>{{ $mytickets->application }}</td>
                                            <!-- <td>{{ $mytickets->created_by}}</td> -->
                                            <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
                                            <!-- <td>{{ $mytickets->contact_email }}</td> -->
                                            <!-- <td>{{ $mytickets->support_category }}</td> -->
                                            <!-- <td>{{ $mytickets->support_description }}</td> -->
                                        </tr>
                                        @empty
                                        <tr>
                                            <p>No Data availabe</p>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-3 server-load">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-header-inside">
                                    Ticket Status
                                </div>
                                <div class="pie-chart-container">
                                    <canvas id="chart_pie" style="width: 100%;"></canvas>
                                </div>
                                <table class="table table-server mt-3">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Tickets</th>
                                        </tr>
                                    </thead>
                                    @can('isAdmin')
                                    <tbody>
                                        <tr>
                                            <td style="width:45%">Closed</td>
                                            <td>{{$closed_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Open</td>
                                            <td>{{$open_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cancelled</td>
                                            <td>{{$cancelled_tickets}}
                                                <!-- <span class="badge badge-info">absolete</span> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endcan
                                    @can('isCertify')
                                    <tbody>
                                        <tr>
                                            <td style="width:45%">Closed</td>
                                            <td>{{$closed_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Open</td>
                                            <td>{{$open_tickets}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cancelled</td>
                                            <td>{{$cancelled_tickets}}
                                                <!-- <span class="badge badge-info">absolete</span> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endcan
                                    @can('isUser')
                                    <tbody>
                                        <tr>
                                            <td style="width:45%">Closed</td>
                                            <td>{{$closed_tickets_mine}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Open</td>
                                            <td>{{$open_tickets_mine}}
                                                <!-- <span class="badge badge-success">normal</span> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cancelled</td>
                                            <td>{{$cancelled_tickets_mine}}
                                                <!-- <span class="badge badge-info">absolete</span> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endcan
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @endcan
        <footer id="page-footer" class="pagefooter">
            <div class="content">
                <div class="row">
                    <div class="copyright col-sm-12 col-md-12 col-lg-6">
                        &copy; 2017 Prudence - Bootstrap Admin Template</div>
                    <div class="footer-nav col-sm-12 col-md-12 col-lg-6">
                        <a href="javascript:void(0)">About</a>
                        <a href="javascript:void(0)">Terms</a></div>
                </div>
            </div>
        </footer>
    </div>

    @include('shared.scripts')
    @can('isAdmin')
    <script>
        var open_tickets = @json($open_tickets);
        var closed_tickets = @json($closed_tickets);
        var cancelled_tickets = @json($cancelled_tickets);
    </script>
    @endcan
    @can('isCertify')
    <script>
        var open_tickets = @json($open_tickets);
        var closed_tickets = @json($closed_tickets);
        var cancelled_tickets = @json($cancelled_tickets);
    </script>
    @endcan
    @can('isUser')
    <script>
        var open_tickets = @json($open_tickets_mine);
        var closed_tickets = @json($closed_tickets_mine);
        var cancelled_tickets = @json($cancelled_tickets_mine);
    </script>
    @endcan
</body>

<!-- Mirrored from html.alfisahr.com/prudence/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Feb 2019 00:47:48 GMT -->

</html>