@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Ticket Number: {{$supportticket->support_ticket_no}} </h2>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="card">
                    <div class="card-header">Attachment</div>
                    <div class="card-body">
                        @if($supportticket->original_filename !== null)
                        <!-- <p>Attachment: {{$supportticket->original_filename}} </p> -->
                        <a href="{{url('uploads/'.$supportticket->filename)}}" target="_blank">
                            <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#exampleModalFive">
                            View Attachment
                            </button>
                        </a>
                        @else
                        <div class="card-header">No Attachment Available</div>
                        @endif

                    </div><!-- .card-body -->
                </div>

            </div><!-- .col -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">

                <!-- Options -->
                <div class="card">
                    <div class="card-header">Details</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Priority</th>
                                    <th>Date Created</th>
                                    <!-- <th>Description</th>
                                    <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$supportticket->company}}</td>
                                    <td>{{$supportticket->application}}</td>
                                    <td>{{$supportticket->priority}}</td>
                                    <td>{{ \Carbon\Carbon::parse($supportticket->created_at)->format('d/M/Y')}}</td>
                                    <!-- <td>The amount of time to delay between automatically cycling an item. If
                                        false, carousel will not automatically cycle.</td> -->
                                </tr>
                            </tbody>
                        </table>
                        <hr />

                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-expanded="true">Title</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-expanded="true">Description</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel">
                                {{$supportticket->support_category}}
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel">
                                {{$supportticket->support_description}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <hr />
        
    </div>
</main>
@include('sweetalert::alert')
@stop