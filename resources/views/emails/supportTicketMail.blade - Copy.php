@component('mail::message')


# {{ $details['title'] }} 


Hello,   
{{$details['message']}}

@component('mail::button', ['url' => $details['url']])
Login to Account
@endcomponent   

Thanks,<br>
{{ config('app.name') }}
@endcomponent
