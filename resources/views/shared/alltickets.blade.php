<table id="example1" class="table table-striped">
    <thead>
        <tr>
            <th>Ticket Number</th>
            <th>Company</th>
            <th>Application</th>
            <th>Ticket Created by</th>
            <th>Creation Date</th>
            <th>Contact Email</th>
            <th>Support Title</th>
            <th>Support Description</th>

        </tr>
    </thead>
    <tbody>
        @forelse($alltickets_mine as $mytickets)
        <tr>
            <td>{{ $mytickets->support_ticket_no }}</td>
            <td>{{ $mytickets->company }}</td>
            <td>{{ $mytickets->application }}</td>
            <td>{{ $mytickets->created_by}}</td>
            <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
            <td>{{ $mytickets->contact_email }}</td>
            <td>{{ $mytickets->support_category }}</td>
            <td>{{ $mytickets->support_description }}</td>
        </tr>
        @empty
        <tr>
            <p>No Data availabe</p>
        </tr>
        @endforelse
    </tbody>
</table>